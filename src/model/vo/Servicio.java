package model.vo;

import java.util.Calendar;

import com.sun.xml.internal.ws.api.FeatureConstructor;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	



	private String tripId;

	private String taxiId;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;
	
	private String trip_start_timestamp;
	
	private String trip_end_timestamp;

	/**
	 * @param tripId
	 * @param taxiId
	 * @param tripSeconds
	 * @param tripMiles
	 * @param tripTotal
	 */
	public Servicio(String tripId, String taxiId, int tripSeconds, double tripMiles, double tripTotal, String fechaInicial, String fechaFinal) {
		super();
		this.tripId = tripId;
		this.taxiId = taxiId;
		this.tripSeconds = tripSeconds;
		this.tripMiles = tripMiles;
		this.tripTotal = tripTotal;
		this.trip_start_timestamp = fechaInicial;
		this.trip_end_timestamp = fechaFinal;
	}



	public Servicio(String trip_id, String taxi_id, int trip_seconds, double trip_miles, double trip_total,
			String trip_start_timestamp2, String trip_end_timestamp2, String company) {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return tripId;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	/**
	 * @return the fechaInicial
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}



	/**
	 * @return the fechaFinal
	 */
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}



	@Override
	public int compareTo(Servicio arg0) {
		
		return tripId.compareTo(arg0.getTripId());
	}



	public void setTimeStart(String trip_start_timestamp2) {
		// TODO Auto-generated method stub
		trip_start_timestamp = trip_start_timestamp2;
		
	}



	public void setTimeEnd(String trip_end_timestamp2) {
		// TODO Auto-generated method stub
		trip_end_timestamp = trip_end_timestamp2;
	}
}
