package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	
	//_________________________________________________________
	//Atributos
	//_________________________________________________________
	
	/**
	 * 
	 */
	private String taxiId;
	
	/**
	 * 
	 */
	private String company;
	
	/**
	 * 
	 */
	private int serviciosRegistrados;
	
	/**
	 * 
	 */
	private double distanciaServicios;
	
	/**
	 * 
	 */
	private double dineroServicios;
	
	//_________________________________________________________
	//Constructor
	//_________________________________________________________
	public Taxi(String pId, String pCompany) 
	{
		taxiId = pId;
		company = pCompany;
		serviciosRegistrados = 0;
		distanciaServicios = 0.0;
		dineroServicios = 0.0;
		
	}
	//_________________________________________________________
	//Metodos
	//_________________________________________________________

	/**
	 * 
	 * @return taxiId
	 */
	public String getTaxiId() {
		return taxiId;
	}

	/**
	 * 
	 * @param taxiId
	 */
	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}

	/**
	 * 
	 * @return
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * 
	 * @param company
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * 
	 * @return
	 */
	public int getServiciosRegistrados() {
		return serviciosRegistrados;
	}

	/**
	 * 
	 * @param serviciosRegistrados
	 */
	public void setServiciosRegistrados() {
		this.serviciosRegistrados ++;
	}

	/**
	 * 
	 * @return
	 */
	public double getDistanciaServicios() {
		return distanciaServicios;
	}

	/**
	 * 
	 * @param distanciaServicios
	 */
	public void setDistanciaServicios(double distanciaServicios) {
		this.distanciaServicios += distanciaServicios;
	}

	/**
	 * 
	 */
	public double getDineroServicios() {
		return dineroServicios;
	}

	/**
	 * 
	 * @param dineroServicios
	 */
	public void setDineroServicios(double dineroServicios) {
		this.dineroServicios += dineroServicios;
	}

	@Override
	public int compareTo(Taxi arg0) {
		return taxiId.compareTo(arg0.getTaxiId());
	}
	

}
