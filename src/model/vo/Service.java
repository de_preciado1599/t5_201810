package model.vo;

import model.vo.Service.dropoff_centroid_location;
import model.vo.Service.pickup_centroid_location;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


	private String company;

	private double dropoff_census_tract;

	private double dropoff_centroid_latitude;

	private dropoff_centroid_location dropoff_centroid_location;

	private double dropoff_centroid_longitude;

	private double dropoff_community_area;

	private double extras;

	private double fare;

	private String payment_type;

	private double pickup_census_tract;

	private double pickup_centroid_latitude;

	private pickup_centroid_location pickup_centroid_location;

	private double pickup_centroid_longitude;

	private double pickup_community_area;

	private String taxi_id;

	private double tips;

	private double tolls;

	private String trip_end_timestamp;

	private String trip_id;

	private double trip_miles;

	private int trip_seconds;

	private String trip_start_timestamp;

	private double trip_total;


	public static class dropoff_centroid_location
	{
		private String type;
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @return the cordinates
		 */
		public double[] getCordinates() {
			return coordinates;
		}

		private double[] coordinates;

		public dropoff_centroid_location(String type, double[] coordinates) {
			this.coordinates = new double[2];
			this.type=type;
			if(coordinates!=null)
			{
				this.coordinates[0] = coordinates[0];
				this.coordinates[1] = coordinates[1];

			}
			else
			{
				this.coordinates[0] = 0.0;
				this.coordinates[1] = 0.0;
			}
		}

		public String toString()
		{
			return type + "Dropoff Cordinates: " + (coordinates[0] + "") + (coordinates[1]+ "");
		}

	}


	public static class pickup_centroid_location
	{
		private String type;
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @return the cordinates
		 */
		public double[] getCordinates() {
			return coordinates;
		}

		private double[] coordinates;
		public pickup_centroid_location(String type, double[] coordinates) {
			this.coordinates = new double[2];
			this.type=type;
			if(coordinates!=null)
			{
				this.coordinates[0] = coordinates[0];
				this.coordinates[1] = coordinates[1];

			}
			else
			{
				this.coordinates[0] = 0.0;
				this.coordinates[1] = 0.0;
			}
		}

		public String toString()
		{
			return type + "Pickup Cordinates: " + (coordinates[0] + "") + (coordinates[1]+ "");
		}

	}



	public Service(String company, double dropoff_census_tract, double dropoff_centroid_latitude, dropoff_centroid_location dropoff_centroid_location ,double dropoff_centroid_longitude, double dropoff_community_area,
			double extras, double fare, String payment_type, double pickup_census_tract,
			double pickup_centroid_latitude, pickup_centroid_location pickup_centroid_location,double pickup_centroid_longitude,
			double pickup_community_area, String taxi_id, double tips, double tolls, String trip_end_timestamp,
			String trip_id, double trip_miles, int trip_seconds, String trip_start_timestamp, double trip_total) {
		this.company = company;
		this.dropoff_census_tract = dropoff_census_tract;
		this.dropoff_centroid_latitude = dropoff_centroid_latitude;
		this.dropoff_centroid_location = dropoff_centroid_location;
		this.dropoff_centroid_longitude = dropoff_centroid_longitude;
		this.dropoff_community_area = dropoff_community_area;
		this.extras = extras;
		this.fare = fare;
		this.payment_type = payment_type;
		this.pickup_census_tract = pickup_census_tract;
		this.pickup_centroid_latitude = pickup_centroid_latitude;
		this.pickup_centroid_location = pickup_centroid_location;
		this.pickup_centroid_longitude = pickup_centroid_longitude;
		this.pickup_community_area = pickup_community_area;
		this.taxi_id = taxi_id;
		this.tips = tips;
		this.tolls = tolls;
		this.trip_end_timestamp = trip_end_timestamp;
		this.trip_id = trip_id;
		this.trip_miles = trip_miles;
		this.trip_seconds = trip_seconds;
		this.trip_start_timestamp = trip_start_timestamp;
		this.trip_total = trip_total;
	}













	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return the dropoff_census_tract
	 */
	public double getDropoff_census_tract() {
		return dropoff_census_tract;
	}


	/**
	 * @return the dropoff_centroid_latitude
	 */
	public double getDropoff_centroid_latitude() {
		return dropoff_centroid_latitude;
	}




		/**
		 * @return the dropoff_centroid_location
		 */
		public dropoff_centroid_location getDropoff_centroid_location() {
			return dropoff_centroid_location;
		}













	/**
	 * @return the dropoff_centroid_longitude
	 */
	public double getDropoff_centroid_longitude() {
		return dropoff_centroid_longitude;
	}













	/**
	 * @return the dropoff_community_area
	 */
	public double getDropoff_community_area() {
		return dropoff_community_area;
	}













	/**
	 * @return the extras
	 */
	public double getExtras() {
		return extras;
	}













	/**
	 * @return the fare
	 */
	public double getFare() {
		return fare;
	}













	/**
	 * @return the payment_type
	 */
	public String getPayment_type() {
		return payment_type;
	}













	/**
	 * @return the pickup_census_tract
	 */
	public double getPickup_census_tract() {
		return pickup_census_tract;
	}













	/**
	 * @return the pickup_centroid_latitude
	 */
	public double getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}













		/**
		 * @return the pickup_centroid_location
		 */
		public pickup_centroid_location getPickup_centroid_location() {
			return pickup_centroid_location;
		}













	/**
	 * @return the pickup_centroid_longitude
	 */
	public double getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}













	/**
	 * @return the pickup_community_area
	 */
	public double getPickup_community_area() {
		return pickup_community_area;
	}













	/**
	 * @return the taxi_id
	 */
	public String getTaxi_id() {
		return taxi_id;
	}













	/**
	 * @return the tips
	 */
	public double getTips() {
		return tips;
	}













	/**
	 * @return the tolls
	 */
	public double getTolls() {
		return tolls;
	}













	/**
	 * @return the trip_end_timestamp
	 */
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}













	/**
	 * @return the trip_id
	 */
	public String getTrip_id() {
		return trip_id;
	}













	/**
	 * @return the trip_miles
	 */
	public double getTrip_miles() {
		return trip_miles;
	}













	/**
	 * @return the trip_seconds
	 */
	public int getTrip_seconds() {
		return trip_seconds;
	}













	/**
	 * @return the trip_start_timestamp
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}













	/**
	 * @return the trip_total
	 */
	public double getTrip_total() {
		return trip_total;
	}













	@Override
	public int compareTo(Service o) {
		return trip_id.compareTo(o.getTrip_id());
	}
}
