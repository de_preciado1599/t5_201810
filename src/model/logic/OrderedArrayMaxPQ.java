package model.logic;

import java.util.Comparator;

/******************************************************************************
 *  Compilation:  javac OrderedArrayMaxPQ.java
 *  Execution:    java OrderedArrayMaxPQ
 *  Dependencies: StdOut.java 
 *  
 *  Priority queue implementation with an ordered array.
 *
 *  Limitations
 *  -----------
 *   - no array resizing 
 *   - does not check for overflow or underflow.
 *  
 *
 ******************************************************************************/

public class OrderedArrayMaxPQ<Key extends Comparable<Key>> {
    private Key[] pq;          // elements
    private int n;             // number of elements
    private Comparator<Key> comparador;

    // set inititial size of heap to hold size elements
    public OrderedArrayMaxPQ(int capacity , Comparator<Key> t) {
        pq = (Key[]) (new Comparable[capacity]);
        n = 0;
        comparador = t;
    }


    public boolean isEmpty() { return n == 0;  }
    public int size()        { return n;       } 
    public Key delMax()      { return pq[--n]; }

    public void insert(Key key) {
        int i = n-1;
        while (i >= 0 && less(key, pq[i])) {
            pq[i+1] = pq[i];
            i--;
        }
        pq[i+1] = key;
        n++;
    }



   /***************************************************************************
    * Helper functions.
    ***************************************************************************/
    private boolean less(Key v, Key w) {
    	
    	return comparador.compare(v, w) <0 ;
    }

   /***************************************************************************
    * Test routine.
    ***************************************************************************/
    public static void main(String[] args) {
    	Comparator<Integer> comparador = new Comparator<Integer>() {
			
			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		};
        OrderedArrayMaxPQ<Integer> pq = new OrderedArrayMaxPQ<Integer>(20 , comparador);
        pq.insert(101);
        pq.insert(3);
        pq.insert(34);
        pq.insert(35);
        while(pq.size()!=0)
        {
        	System.out.println(pq.delMax());
        }
    }

}
